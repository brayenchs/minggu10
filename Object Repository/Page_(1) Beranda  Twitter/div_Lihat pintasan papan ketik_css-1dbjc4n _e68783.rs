<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Lihat pintasan papan ketik_css-1dbjc4n _e68783</name>
   <tag></tag>
   <elementGuidId>2d0e2a60-0c7f-4a88-96b7-0a4dc07bb06c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='react-root']/div/div/div[2]/header/div/div/div/div/div[2]/nav/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>css-1dbjc4n r-1awozwy r-zv2cs0 r-sdzlij r-18u37iz r-1777fci r-dnmrzs r-1sp51qo r-o7ynqc r-6416eg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;react-root&quot;)/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-13awgt0 r-12vffkv&quot;]/div[@class=&quot;css-1dbjc4n r-18u37iz r-1pi2tsx r-13qz1uu r-417010&quot;]/header[@class=&quot;css-1dbjc4n r-obd0qt r-16y2uox r-1g40b8q&quot;]/div[@class=&quot;css-1dbjc4n r-f9dfq4&quot;]/div[@class=&quot;css-1dbjc4n r-aqfbo4 r-1pi2tsx r-1xcajam r-ipm5af&quot;]/div[@class=&quot;css-1dbjc4n r-1pi2tsx r-1wtj0ep r-1rnoaur r-d9fdf6 r-f9dfq4&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-d0pm55 r-1bymd8e r-13qz1uu&quot;]/nav[@class=&quot;css-1dbjc4n r-1awozwy r-eqz5dr&quot;]/div[@class=&quot;css-18t94o4 css-1dbjc4n r-1habvwh r-6koalj r-eqz5dr r-16y2uox r-1ny4l3l r-oyd9sg r-13qz1uu&quot;]/div[@class=&quot;css-1dbjc4n r-1awozwy r-zv2cs0 r-sdzlij r-18u37iz r-1777fci r-dnmrzs r-1sp51qo r-o7ynqc r-6416eg&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='react-root']/div/div/div[2]/header/div/div/div/div/div[2]/nav/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat pintasan papan ketik'])[1]/following::div[41]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Untuk melihat pintasan papan ketik, tekan tanda tanya'])[1]/following::div[41]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beranda'])[1]/preceding::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Tweet baru'])[1]/preceding::div[16]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/div/div</value>
   </webElementXpaths>
</WebElementEntity>
