<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Uji Tugas Tambah Produk</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>062511fc-62e9-4765-817b-bfb024c98bc4</testSuiteGuid>
   <testCaseLink>
      <guid>f8cf476d-cbe6-4251-ad92-6c2e60b6d264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tambah Produk Baru - Tugas Minggu 10</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test Tambah Produk 10</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>780216bb-9df8-4b7f-b98d-42a7ff1937b3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>2dc4167a-aa0a-4112-ad46-6aede7f46238</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>product</value>
         <variableId>c2067e54-e000-4693-b27d-b04545bf0e77</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>price</value>
         <variableId>66c767b0-df5b-4502-99f8-1465eb14b9a1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>32d10c8d-2c51-4034-93c8-1a2bbf2d0762</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>description</value>
         <variableId>86ff3ac9-1fa1-4b5f-87db-ee54372b6b3a</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
