<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Basic test suite for CURA</description>
   <name>Basic</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f7bab810-60b5-4f30-8a28-06e0348cb07c</testSuiteGuid>
   <testCaseLink>
      <guid>ebf305cd-4750-4da8-8ebc-7961d955dfb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Basic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c22cf620-b26a-4084-b041-0d60c8a6f15f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web Ukdw</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0af56273-6ef7-4f29-a57c-64e21cd76e49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web Moodle Ukdw</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>347f81d5-7887-4265-a944-fc827ef2299d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web SSAT Ukdw</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5297eee4-d881-4c62-85f9-6325d2de1128</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Web Twitter</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
